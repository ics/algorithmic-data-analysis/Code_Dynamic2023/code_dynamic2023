# -*- coding: utf-8 -*-

"""RUL Prediction Manager

This script analyses and visualises the RUL predictions of aircraft engines using statistical methods, and processes
data from CNN models to evaluate their accuracy.

Usage:
------
1. Set the `data_subset` variable to specify the CMAPPS subfolder.
2. Customise additional parameters like `r_early` and `max_RUL` as needed.
3. Set the Boolean settings `plot_violins` and `log_engine_data` accordingly.
4. When plotting the violin plots, choose the `engine_to_plot`.
5. Run the script.

"""

import os
import pickle
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import matplotlib
import pandas as pd


# Local settings to customise the output and behaviour of this script.

# The CMAPPS subfolder that should be used in the script.
# Choices: FD001, FD002, FD003, FD004 or "all" (for all four data sets together)
data_subset = "FD001"

# The engine that should be plotted into a violin plot - engine numbers start at 1.
# The engine should be present in the "engine_true_RUL" dictionary after loading the engine data.
engine_to_plot = "FD001_1"

# A parameter for the piece-wise linear RUL target function. If r_early is set to x, then the target RUL is x flight
# cycles when the actual RUL is larger than x flight cycles.
r_early = 125

# When determining the PDF distributions of each engine, the max_RUL indicates how many columns with a possible RUL
# value exist (from zero to max_RUL flights), with for each column the probability.
max_RUL = 200

# Whether the RUL predictions should be plotted into the violon plots.
plot_violins = True

# Whether engine data should be printed to the console.
log_engine_data = True


# Initialise global variables to store the engine data that will be loaded in.

# A dictionary with as keys the labels of the engines, and as values a list with all true RUL values.
# The true RUL values are sorted in descending order.
engine_true_RUL = {}
# A dictionary with as keys the names of the engines, and as values a list with all mean predicted RULs.
engine_mean_prediction = {}
# A dictionary with as keys the names of the engines.
# The values are a list, with for each true RUL a list with all individual predictions.
engine_all_predictions = {}


def load_engine_data():
    """
    Loads engine data [RUL prognostics] from specified files for the specified instance, or all instances.

    The engines are named as instance_number of the engine, where the number of the engine starts at 1.

    Raises
    ------
    ValueError
        If the selected 'instance' value is invalid.
    """
    global engine_true_RUL, engine_mean_prediction, engine_all_predictions

    if data_subset in ["FD001", "FD002", "FD003", "FD004"]:
        engine_true_RUL = load_pickle_file(f"./data/{data_subset}/{data_subset}_engine_true_RUL")
        engine_mean_prediction = load_pickle_file(f"./data/{data_subset}/{data_subset}_engine_mean_prediction")
        engine_all_predictions = load_pickle_file(f"./data/{data_subset}/{data_subset}_engine_all_predictions")

        print(f"This dataset contains {len(engine_true_RUL)} engines.")
    elif data_subset == "all":
        for i in ["FD001", "FD002", "FD003", "FD004"]:
            engine_true_RUL_i = load_pickle_file(f"./data/{i}/{i}_engine_true_RUL")
            engine_mean_prediction_i = load_pickle_file(f"./data/{i}/{i}_engine_mean_prediction")
            engine_all_predictions_i = load_pickle_file(f"./data/{i}/{i}_engine_all_predictions")

            engine_true_RUL.update(engine_true_RUL_i)
            engine_mean_prediction.update(engine_mean_prediction_i)
            engine_all_predictions.update(engine_all_predictions_i)

        print(f"All datasets together contain {len(engine_true_RUL)} engines.")
    else:
        raise ValueError(
            "Invalid 'data_subset' value. The available dataset subfolders are 'FD001', 'FD002', 'FD003', 'FD004', or"
            " 'all' for all four subfolders.")


def load_pickle_file(file_path):
    """
    Reads the data from a (pickle) file.

    Parameters
    ----------
    file_path : str
        The path to the data file.

    Returns
    -------
    Any
        The data read from the data file.

    Raises
    ------
    FileNotFoundError
        If the specified file does not exist.
    """
    # If the required data does not exist, emit an informative error.
    if not os.path.exists(file_path):
        path_parts = file_path.split("/")
        subset = path_parts[2]
        file_name = path_parts[3]
        raise FileNotFoundError(f"Could not find the {file_name} file in the {subset} subfolder.")

    with open(file_path, "rb") as file:
        data = pickle.load(file)

    return data


def determine_PDF_distributions():
    """
    Determines the PDF distribution of each engine.

    This function saves the PDF distributions to a pandas dataframe, with as colmns the engime name, the true RUL, and the mean predicted RUL.

    Raises
    ------
    ValueError
        If the largest prediction exceeds the maximum RUL.
    """
    columns = ["engine_name", "subset", "true_rul", "true_rul_R_early", "mean_rul"]
    columns.extend(list(range(0, max_RUL + 1, 1)))
    all_pdfs = pd.DataFrame(columns=columns)

    for engine in engine_true_RUL.keys():
        if log_engine_data:
            print(f"We're at engine {engine}...")

        true_RULs = engine_true_RUL.get(engine)
        true_RULs = [len(true_RULs) - i - 1 if true_RUL == r_early else true_RUL for i, true_RUL in
                     enumerate(true_RULs)]
        mean_RUL = engine_mean_prediction.get(engine)
        all_predictions = engine_all_predictions.get(engine)
        max_predicted_RUL = int(np.max(all_predictions))

        if log_engine_data:
            print(f" The maximum predicted RUL is {max_predicted_RUL}.")

        if max_predicted_RUL > max_RUL:
            raise ValueError(f"The largest prediction is larger than the maximum RUL of {max_RUL}.")

        pdfs_engine = []

        # Now loop over the true RULs, where each RUL is a row.
        for index, RUL in enumerate(true_RULs):
            mean = mean_RUL[index]

            predictions = np.array(sorted(all_predictions[index]))

            pdf = {predicted_RUL: np.sum((predictions >= predicted_RUL) & (predictions < predicted_RUL + 1)) / len(
                predictions)
                   for predicted_RUL in range(0, max_RUL + 1)}

            # Add the PDF dictionary to the dataframe.
            pdf.update({
                "engine_name": engine,
                "true_rul": RUL,
                "mean_rul": mean,
                "true_rul_R_early": min(r_early, RUL),
                "subset": engine[:5]
            })

            pdfs_engine.append(pdf)

        all_pdfs = all_pdfs._append(pdfs_engine, ignore_index=True)


    file_name = f"./data/PDF/pdf_{data_subset}"
    file_path = os.path.dirname(file_name)
    if not os.path.exists(file_path):
        os.makedirs(file_path)

    with open(file_name, "wb") as file:
        pickle.dump(all_pdfs, file)


def plot_RUL_predictions():
    """
    Plots the RUL predictions over time for one engine in a violin plot.

    Raises
    ------
    ValueError
        If the specified engine is not found in the data.
    """
    if engine_to_plot not in engine_true_RUL:
        raise ValueError(f"Engine '{engine_to_plot}' was not found in the engine_true_RUL data.")

    fig, ax = plt.subplots(figsize=(25, 6))
    font_size = 20

    # Plot the true RULs.
    true_RULs = engine_true_RUL.get(engine_to_plot)
    correct_plot, = ax.plot(true_RULs, label="True RUL", lw=3, color="blue")

    step_interval = 10
    count = len(true_RULs)

    # print(len(true_RULs))
    ax.set_xticks(range(0, count + 1, step_interval))
    labels_x = [i for i in range(count, -1, -step_interval)]
    ax.set_xticklabels(labels_x)
    ax.set_xlim(r_early - step_interval, count)

    # Plot the mean RULs.
    mean_RULs = engine_mean_prediction.get(engine_to_plot)
    mean_plot, = ax.plot(mean_RULs, label="Mean prediction", marker=".", color="red")

    distribution = engine_all_predictions.get(engine_to_plot)

    # Show the predicted distributions using coloured violins.
    violin_parts = ax.violinplot(distribution, list(range(len(mean_RULs))), showmeans=False, showextrema=True,
                                 showmedians=False, widths=1.)

    for vp in violin_parts['bodies']:
        vp.set_facecolor('green')
        vp.set_edgecolor('green')
        vp.set_linewidth(1)
        vp.set_alpha(0.6)

    for partname in ('cbars', 'cmins', 'cmaxes'):
        vp = violin_parts[partname]
        vp.set_edgecolor('black')
        vp.set_linewidth(1)
        vp.set_alpha(1)

    ax.set_xlabel("Actual RUL of the engine", fontsize=font_size)
    ax.set_ylabel("RUL (flights)", fontsize=font_size)
    min_max = Line2D([], [], color='black')
    rect = matplotlib.patches.Rectangle((120, 0), 1, 0.5, facecolor="green")

    ax.legend(handles=[correct_plot, mean_plot, min_max, rect],
              labels=['Actual RUL', 'Mean predicted RUL', 'Min. and Max.', 'PDF'], fontsize=font_size,
              loc='upper center', bbox_to_anchor=(0.5, -0.25), ncol=4)

    engine = engine_to_plot.split("_")[1]
    subfolder = engine_to_plot.split("_")[0]
    fig.suptitle(f"Engine {engine} of {subfolder}", fontsize=font_size)
    fig.tight_layout()

    # Save the violin plot to the "figures" folder.
    # If the folder does not yet exist locally, the program will automatically generate it.
    file_name = f"./figures/violin_plots/violin_plot_{engine_to_plot}"
    file_path = os.path.dirname(file_name)
    if not os.path.exists(file_path):
        os.makedirs(file_path)

    plt.savefig(file_name, dpi=400)
    plt.close()


def main():
    load_engine_data()

    determine_PDF_distributions()
    print("Successfully determined the maximum RUL of all engines.")

    if plot_violins:
        plot_RUL_predictions()


if __name__ == "__main__":
    main()
