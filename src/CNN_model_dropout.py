# -*- coding: utf-8 -*-

"""CNN models for RUL predictions

This script is designed to train and test Convolutional Neural Network (CNN) models for predicting the Remaining
Useful Life (RUL) of aircraft engines. The models utilise Monte Carlo dropout to provide estimates in predictions,
optionally simulating different operational scenarios. For each C-MAPSS subfolder FD001 to FD004, a model is created.

The script requires the normalised datasets made using the other available scrips in the repository, for details
please see the readme.md or the accompanying paper by Mitici et al.

Usage:
------
1. Prepare the data for the RUL predictions. For more information, see the "CNN_data_preparation.py" file.
2. Update the settings in the "settings.json" file as desired.
3. Run the script to train or test the model.

"""

import json
import matplotlib.pyplot as plt
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dense, Flatten, Activation
from tensorflow.keras.layers import Dropout
from tensorflow.keras.optimizers import Adam
from tensorflow.keras import activations
from tensorflow.keras.callbacks import ModelCheckpoint, ReduceLROnPlateau, LearningRateScheduler

from input_validator import validate_settings
from CNN_data_preparation import input_format_prep
from metrics_RUL_predictions import test_montecarlo_output


with open("settings.json", "r") as settings_file:
    settings = json.load(settings_file)

# Whether to train the model, or evaluate the results of the model. These values are mutually exclusive, and
# one, and only one, must be set to "true" at all times.
train_model = settings["training"]
test_model = settings["testing"]

# The dropout rates for each data subfolder in each layer of the CNN. The default is a 50% dropout rate to combat
# overfitting the model.
dropout_rate = settings["dropout_rate"]

# The sensors that should be included. By default, only sensors with non-constant measurements are considered.
sensors_to_use = settings["sensors_to_use"]

# A parameter for the piece-wise linear RUL target function. If r_early is set to x, then the target RUL is x flight
# cycles when the actual RUL is larger than x flight cycles.
r_early = settings["r_early"]

# The amount of samples propogating through the CNN in one step.
# A smaller batch size: batch size < number of samples requires less memory-complexity and it performs faster,
# at the cost of estimation accuracy.
batch_size = settings["batch_size"]

# The amount of "loops" a CNN will make while training the data.
epochs = settings["no_epochs"]

# The number of past flight cycles included, specified for each data subfolder. In case the window size is set higher
# than the number of historical flight cycles, zero padding is applied.
window_size = settings["window_size"]

# The number of passes through the neural network ("M" in the paper, table 2).
test_runs = settings["no_test_runs"]

# List of points in time to plot the RUL for (example in Figure 4 of the paper).
flight_cycles = settings["plot_rul_flight_cycles"]


def main():
    # Ensure that no settings have invalid values, and handle potential errors accordingly.
    validate_settings(settings)

    # Choose which of the subfolders should be used.
    # Default is set to all sets (FD001 = 1, FD002 = 2, FD003 = 3, FD004 = 4).
    for i in settings["c-mapss_subfolders"]:
        data_subset = f"FD00{str(i)}"
        model_name = f"{data_subset}_sim_mc_dropout"
        print("Initialising " + model_name + "...\n")

        # Determine what sensors were excluded for the algorithm.
        # By default, 7 out of 21 sensors are excluded because they do not bring any relevant results.
        all_sensors = settings["all_sensors"]
        skipped_sensors = [x for x in all_sensors if x not in sensors_to_use]

        # Instead of the predefined initalisers in the layers, a general initialiser can be chosen from the following
        # source: https://keras.io/api/layers/initializers/.

        # "relu" activation may be considered for all layers if non-normalised data is expected. Otherwise, the
        # standard "tanh" can be used, which is the hyperbolic tangent function.
        # Replace "Activation(activations.tanh)" with "Activation(activations.relu)" on each layer to swap them.

        # Definition of the model with all layers and activation.
        model = Sequential([
            # Layer 1
            Conv2D(10, (1, 10), input_shape=(len(sensors_to_use), window_size, 1), padding="same",
                   kernel_initializer="glorot_normal"),
            Activation(activations.tanh),

            # Layer 2
            Conv2D(10, (1, 10), padding="same", kernel_initializer="glorot_normal"),
            Activation(activations.tanh),
            MCDropout(dropout_rate),

            # Layer 3
            Conv2D(10, (1, 10), padding="same", kernel_initializer="glorot_normal"),
            Activation(activations.tanh),
            MCDropout(dropout_rate),

            # Layer 4
            Conv2D(10, (1, 10), padding="same", kernel_initializer="glorot_normal"),
            Activation(activations.tanh),
            MCDropout(dropout_rate),

            # Layer 5
            Conv2D(1, (1, 3), padding="same", kernel_initializer="glorot_normal"),
            Activation(activations.tanh),
            MCDropout(dropout_rate),

            # Flatten the layer from an n-dimsensional output from the convulutional layers to a one-dimsensional
            # input for the Dense layers.
            Flatten(),
            MCDropout(dropout_rate),

            # Dense layer for further processing the flattened output.
            Dense(100, activation="tanh", kernel_initializer="glorot_normal"),
            MCDropout(dropout_rate),
            # Output layer for the CNN model, providing a single, continuous value.
            Dense(1, activation="relu", kernel_initializer="glorot_normal")
        ])

        # Prepare the final model input and targets from the normalised data files.
        inp = input_format_prep(data_subset, window_size, skipped_sensors)
        test_samples = inp[0]  # sensor readings used to predict the RUL at a certain cycle.
        target_ruls = inp[1]  # target RUL values corresponding to each input sample.

        # Print relevant information to the console.
        print("The dimensions of y are ", len(target_ruls))
        print("with type ", type(target_ruls))
        print(target_ruls[0])

        print("The dimensions of x are ", test_samples.shape)
        print("the type of x is ", type(test_samples))
        print("The length of X is ", len(test_samples))

        # The main training loop.
        if train_model is True:
            opt_adam = Adam()  # Adam optimiser: https://keras.io/api/optimizers/adam/

            model.compile(optimizer=opt_adam, loss='mean_squared_error', metrics=["mae"])

            mcp_save = ModelCheckpoint("./weights\\" + model_name + ".keras", save_best_only=True,
                                       monitor="val_loss", mode="min", verbose=1)

            # Reduce the learning rate after 10 consecutive epochs with no improvement in the loss.
            # The learning rate can be fixed by commenting the line below, and uncommenting the two below it.
            reduce_lr_loss = ReduceLROnPlateau(monitor="val_loss", factor=0.5, patience=10, min_lr=0.0000001,
                                               verbose=1, min_delta=1e-4, mode="auto")
            # scheduler = LearningRateScheduler()
            # reduce_lr_loss = LearningRateScheduler(scheduler)

            # Train the model using the provided data, and save its History object, which contains data about the
            # model's performance.
            history = model.fit(test_samples, target_ruls, batch_size=batch_size, epochs=epochs, callbacks=[mcp_save, reduce_lr_loss],
                                validation_split=0.2, verbose=1)

            plot_statistics(history)

        # The maing testing loop.
        # Evaluates the results for the models that are used to generate the RUL predictions.
        if test_model:
            # Try to load the model weights; if the model has not been trained yet, the program will exit.
            try:
                model.load_weights("./weights/" + model_name + ".keras")
            except FileNotFoundError:
                # print("No weights were found, please train the model first.")
                raise FileNotFoundError("No weights were found, please train the model first.")

            print(model.summary())

            test_montecarlo_output(model, test_samples, target_ruls, test_runs, flight_cycles, data_subset)


class MCDropout(Dropout):
    """The Monte Carlo dropout function from the regular keras dropout function."""

    def call(self, inputs):
        return super().call(inputs, training=True)


def scheduler():
    """
    Creates a learning rate scheduler to fix the learning rate.

    Returns
    -------
    float
        The learning rate scheduler.
    """
    if epochs < 200:
        return 0.001
    else:  # for the last 50 epochs
        return 0.0001


def plot_statistics(history):
    """
    Plots the loss and MAE (Mean Absolute Error) against the number of epochs.

    Parameters
    ----------
    history : list
        A logbook of the training of the CNN, containing the performance metrics.
    """
    # Collect the loss and MAE (Mean Absolute Error).
    train_loss = history.history['loss']
    train_mean_abs_error = history.history['mae']
    val_loss = history.history['val_loss']
    val_mean_abs_error = history.history['val_mae']

    # Plot the loss and MAE against the number of epochs.
    xrange = range(1, len(train_loss) + 1)

    plt.figure("Training and Validation loss (MSE) vs. number of epochs:")
    plt.plot(xrange, train_loss, '-', label='Training loss (MSE)')
    plt.plot(xrange, val_loss, '-', label='Validation loss (MSE)')
    plt.grid()
    plt.legend()
    plt.show()

    plt.figure("Training and Validation Mean Abs. Error (MAE) vs. number of epochs:")
    plt.plot(xrange, train_mean_abs_error, 'r-', label='Training (MAE)')
    plt.plot(xrange, val_mean_abs_error, 'b-', label='Validation loss (MAE)')
    plt.grid()
    plt.legend()
    plt.show()


if __name__ == "__main__":
    main()
