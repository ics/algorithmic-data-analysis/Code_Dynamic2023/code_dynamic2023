# -*- coding: utf-8 -*-

"""Utility for RUL Predictions

This script provides a suite of functions designed to evaluate the performance and reliability of predictive models
of the CNN models from CNN_model_dropout.py. It includes methods to calculate metrics such as the coverage and
reliability scores, along with utilities for Monte Carlo simulations and statistical analysis.

The script contains statistical approaches to assess predictions, focusing on accuracy and calibration against actual
outcomes. It is part of a larger repository that includes other scripts for data normalization and model training,
detailed in the README.md and relevant publications.

"""

import os
import numpy as np
import matplotlib.pyplot as plt
import sys
import random as rd
import tensorflow as tf
import tqdm
import math
import pickle


def compute_coverage(true_RULs, RUL_distributions, alpha):
    """
    Computes the alpha-coverage and corresponding alpha-mean width.

    Parameters
    ----------
    true_RULs: dict
        A dictionary with for each test instance (key, integer), the true RUL (value). 
    RUL_distributions : dict
        A dictionary with for each test instance (key, integer), a list (value) with all RUL predictions
        of this test instance. true_RULs and RUL distributions should have the same set of keys.
    alpha : float between 0 (included) and 1 (included)
        The desired width of the credible interval.

    Returns
    -------
    coverage : float between 0 (included) and 1 (included)
        The coverage belonging to alpha.
    mean_width : float
        The mean width of the credible interval belonging to alpha.

    Raises
    ------
    ValueError
        If `alpha` is not between 0 and 1 (inclusive).
    """
    if not (0 <= alpha <= 1):
        raise ValueError("Alpha should be between 0 and 1 (inclusive).")

    # Initialize the parameters of the credible interval
    total_width = 0
    in_ci = 0  # The number of components for which the true RUL falls within the credible interval
    percentile_lower = 0.5 - 0.5 * alpha
    percentile_higher = 0.5 + 0.5 * alpha

    # Check for each test instance i if the true RUL falls inside or outside the credible interval.
    for instance in true_RULs.keys():
        # Get the probability dstributions of the RUL test instance i, and the true RUL.
        distribution = RUL_distributions.get(instance)
        true_RUL = true_RULs.get(instance)
        distribution.sort()
        number_of_predictions = len(distribution)

        # Get the indices of the RUL predictions belonging to the considered percenticles.
        index_lower = max(0, int(percentile_lower * number_of_predictions) - 1)
        index_higher = int(percentile_higher * number_of_predictions) - 1
        lower_bound_ci = distribution[index_lower]
        upper_bound_ci = distribution[index_higher]

        # Check if the true RUL is within the credible interval.
        if lower_bound_ci <= true_RUL <= upper_bound_ci:
            in_ci += 1

        # Update the total width of all credible interval.
        total_width += (upper_bound_ci - lower_bound_ci)

    # Calculate the coverage and the mean width of the credible interval.
    coverage = in_ci / len(true_RULs.keys())
    mean_width = total_width / len(true_RULs.keys())
    return coverage, mean_width


def compute_mean_variance(RUL_distributions, number_of_runs):
    """
    Computes the mean variance of the RUL distributions.

    RUL_distributions : dict
        A dictionary with for each test instance (key, integer), a list (value) with all RUL predictions of this test
        instance.
    number_of_runs : int
        The number of prediction runs.

    Returns
    -------
    mean_variance : float
        The mean variance of the RUL distributions.
    mean_std_dev : float
        The mean standard deviation of the RUL distributions.
    """
    total_variance = 0
    total_std_dev = 0

    for i in RUL_distributions.keys():
        distribution = RUL_distributions[i]
        mean = np.mean(np.array(distribution))

        # Get the variance of the prediction.
        variance = 0
        for j in range(0, len(distribution), 1):
            variance = variance + (distribution[j] - mean) ** 2
        variance = variance / number_of_runs
        std_dev = math.sqrt(variance)

        total_variance = total_variance + variance
        total_std_dev += std_dev

    mean_variance = total_variance / len(RUL_distributions.keys())
    mean_std_dev = total_std_dev / len(RUL_distributions.keys())

    return mean_variance, mean_std_dev


def compute_area_under(x1, x2, f1, f2):
    """
    Computes the area between the ideal curve and the reliability curve, between x1 and x2. Here, the reliability
    curve is under the ideal curve between x1 and x2.

    Parameters
    ----------
    x1 : float
        The start value of alpha.
    x2 : float
        The end value of alpha.
    f1 : float
        The coverage at alpha = x1.
    f2 : float
        The coverage at alpha = x2.

    Returns
    -------
    float
        The area between the ideal curve and the reliability curve, between x_1 and x_2.
    """
    area = (x2 - f2) * (x2 - x1) - 0.5 * (x2 - x1) * (x2 - x1)
    area += 0.5 * (x2 - x1) * (f2 - f1)
    return area


def compute_area_above(x1, x2, f1, f2):
    """
    Computes the area between the ideal curve and the reliability curve, between x1 and x2. Here, the reliability
    curve is above the ideal curve between x1 and x2.

    Parameters
    ----------
    x1 : float
        The start value of alpha.
    x2 : float
        The end value of alpha.
    f1 : float
        The coverage at alpha = x1.
    f2 : float
        The coverage at alpha = x2 .
    Returns
    -------
    float
        The area between the ideal curve and the reliability curve, between x1 and x2.
    """
    area = (f1 - x1) * (x2 - x1) - 0.5 * (x2 - x1) * (x2 - x1)
    area += 0.5 * (x2 - x1) * (f2 - f1)
    return area


def compute_reliability_score(true_RULs, RUL_distributions):
    """
    Computes the reliability scores (under, over, and total), and optionally plots the reliability diagram.

    Parameters
    ----------
    true_RULs: dict
        A dictionary with for each test instance (key, integer), the true RUL (value). 
    RUL_distributions : dict
        A dictionary with for each test instance (key, integer), a list (value) with all RUL predictions of this test
        instance. true_RULs and RUL distributions should have the same set of keys.

    Returns
    -------
    tuple[float, float, float]
        The total reliability score.
        The reliability score, an underestimation of uncertainty.
        The reliability score, an overestimation of uncertainty.
    """
    step_size = 0.01

    # Calculate the reliability and ideal curves.
    reliability_curve = []
    for alpha in np.arange(0, 1 + sys.float_info.epsilon, step_size):  # one is included
        alpha_coverage = compute_coverage(true_RULs, RUL_distributions, alpha)[0]
        reliability_curve.append(alpha_coverage)

    # Calculate the reliability score.
    RS_under = 0  # underestimation of the uncertainty
    RS_over = 0  # overestimation of the uncertainty

    for alpha in np.arange(0, 1, step_size):
        next_alpha = alpha + step_size
        coverage_alpha = compute_coverage(true_RULs, RUL_distributions, alpha)[0]
        coverage_next_alpha = compute_coverage(true_RULs, RUL_distributions, next_alpha)[0]

        # If the reliability curve is beneath the ideal curve:
        if coverage_alpha <= alpha and coverage_next_alpha <= next_alpha:
            surface = compute_area_under(alpha, next_alpha, coverage_alpha, coverage_next_alpha)
            RS_under += surface

        # If the reliability curve is above the ideal curve:
        elif coverage_alpha >= alpha and coverage_next_alpha >= next_alpha:
            surface = compute_area_above(alpha, next_alpha, coverage_alpha, coverage_next_alpha)
            RS_over += surface

        # If the reliability curve starts under the ideal curve, and ends above the ideal curve:
        elif coverage_alpha <= alpha and coverage_next_alpha >= next_alpha:
            # Find the place where the reliability curve crosses the ideal curve.
            dy = coverage_next_alpha - coverage_alpha
            a = dy / step_size
            alpha_cross = (coverage_alpha - a * alpha) / (1 - a)
            coverage_cross = alpha_cross

            # Calculate the surface under the ideal curve.
            surface_under = compute_area_under(alpha, alpha_cross, coverage_alpha, coverage_cross)
            RS_under += surface_under
            # Calculate the surface above the ideal curve.
            surface_above = compute_area_above(alpha_cross, next_alpha, coverage_cross, coverage_next_alpha)
            RS_over += surface_above

        # If the reliability curve starts above the ideal curve, and ends under the ideal curve:
        elif coverage_alpha >= alpha and coverage_next_alpha <= next_alpha:
            dy = coverage_next_alpha - coverage_alpha
            a = dy / step_size
            alpha_cross = (coverage_alpha - a * alpha) / (1 - a)
            coverage_cross = alpha_cross

            surface_above = compute_area_above(alpha_cross, next_alpha, coverage_cross, coverage_next_alpha)
            RS_over += surface_above
            surface_under = compute_area_under(alpha, alpha_cross, coverage_alpha, coverage_cross)
            RS_under += surface_under

    RS_total = RS_under + RS_over
    return RS_total, RS_under, RS_over


def plot_histograms(name, flight_cycles, true_RULs, RUL_distributions, mean_RULs):
    """
    Plots the histograms of the probability distributions of RUL output of the CNN.

    Parameters
    ----------
    name : str
        The currently selected C-MAPSS subfolder.
    flight_cycles : list
        The points in time to plot the RUL for (example in Figure 4 of the paper).
    true_RULs : list
        The absolute RULs until failure.
    RUL_distributions : list
        The RUL distributions.
    mean_RULs : list
        The means of the RUL distributions.
    """
    for prediction in flight_cycles:
        fig, ax = plt.subplots()
        font_size = 16
        bin_width = 5

        predictions = RUL_distributions[prediction]
        true_RUL = true_RULs[prediction]
        mean_RUL = mean_RULs[prediction]

        # Plot a histogram with the predictions.
        ax.hist(predictions, density=True, bins=np.arange(min(predictions), max(predictions) + bin_width, bin_width),
                color="lightcoral", ec="lightcoral")

        # Plot a vertical line at the true RUL.
        ax.axvline(x=true_RUL, lw=3.2, label="Actual RUL", c='b')
        ax.axvline(x=mean_RUL, lw=3.2, label="Mean predicted RUL", c='r')

        ax.set_ylabel('Probability', fontsize=font_size)
        ax.set_xlabel('RUL (flight cycles)', fontsize=font_size)

        ax.spines["right"].set_visible(False)
        ax.spines["top"].set_visible(False)

        ax.tick_params(axis='x', labelsize=font_size)
        ax.tick_params(axis='y', labelsize=font_size)

        ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.25), ncol=2, fontsize=font_size - 2)

        fig.tight_layout()
        file_name = f"./figures/histograms/hist_{name}_{prediction}"
        file_path = os.path.dirname(file_name)
        if not os.path.exists(file_path):
            os.makedirs(file_path)

        plt.savefig(file_name, dpi=400)


def save_prediction_data(file_name, engine_true_RUL, engine_mean_prediction, engine_all_predictions):
    """
    Saves prediction data to their respective files.

    Parameters
    ----------
    file_name : str
        The file path to save the prediction data to.
    engine_true_RUL : dict
        The true RUL values for the engines.
    engine_mean_prediction : dict
        The mean RUL predictions for the engines.
    engine_all_predictions : dict
        All RUL predictions for the engines.
    """
    file_path = os.path.dirname(file_name)

    # Safety check if the respective folder does not yet exist.
    if not os.path.exists(file_path):
        os.makedirs(file_path)

    # Helper function for saving pickle files.
    def save_pickle(data, suffix):
        full_file_name = f"{file_name}_test_{suffix}"
        with open(full_file_name, "wb") as file:
            pickle.dump(data, file)

    # Save each of the prediction data dictionaries
    save_pickle(engine_true_RUL, "engine_true_RUL")
    save_pickle(engine_mean_prediction, "engine_mean_prediction")
    save_pickle(engine_all_predictions, "engine_all_predictions")


def test_montecarlo_output(model, test_samples, target_ruls, number_of_runs, flight_cycles, name):
    """
    Computes the output values of the model, and uses some KPIs to show the results.

    Parameters
    ----------
    model : tf.keras.Model
        The trained model to be tested.
    test_samples : np.ndarray
        The prepared input samples for the CNN model, where each sample is a sequence of sensor measurements over a
        specified window size.
    target_ruls : np.ndarray
        The target RUL values corresponding to each input sample.
    number_of_runs : int
        The number of Monte Carlo runs to perform.
    flight_cycles : list
        The flight cycles for which to plot the RUL.
    name : str
        The name of the CMAPPS subfolder.
    """
    mc_predictions = []
    true_RULs, RUL_distributions, mean_RULs = {}, {}, {}
    RMSE, MAE = 0, 0

    rd.seed(7042018)
    tf.random.set_seed(7042018)

    # Predict the RUL value for each Monte Carlo run.
    for _ in tqdm.tqdm(range(number_of_runs)):
        y_p = np.rint(model.predict(test_samples))
        mc_predictions.append(y_p)

    # Calculate the mean of the predictions, the actual predictions, and the standard deviation of the predictions.
    for prediction in range(len(mc_predictions[0])):
        all_predictions = [mc_predictions[j][prediction][0] for j in range(len(mc_predictions))]
        RUL_distributions[prediction] = all_predictions
        true_RULs[prediction] = target_ruls[prediction]
        mean_RULs[prediction] = np.mean(np.array(all_predictions))
        RMSE += (target_ruls[prediction] - np.mean(np.array(all_predictions))) ** 2
        MAE += abs(target_ruls[prediction] - np.mean(np.array(all_predictions)))

    coverage_0_5, mean_width_0_5 = compute_coverage(true_RULs, RUL_distributions, 0.5)
    coverage_0_9, mean_width_0_9 = compute_coverage(true_RULs, RUL_distributions, 0.9)
    coverage_0_95, mean_width_0_95 = compute_coverage(true_RULs, RUL_distributions, 0.95)
    RS_total, RS_under, RS_over = compute_reliability_score(true_RULs, RUL_distributions)

    mean_var, mean_std = compute_mean_variance(RUL_distributions, number_of_runs)

    num_predictions = len(target_ruls)

    # Print the most important quick KPI's when testing the CNN.
    print(f"\nThere are a total of {num_predictions} predictions.")
    print(f"The reliability score (under) is {RS_under}")
    print(f"The reliability score (over) is {RS_over}")
    print(f"The total reliability score is {RS_total}")
    print(f"The coverage at alpha = 0.5 is {coverage_0_5}")
    print(f"The mean width at 0.5 is {mean_width_0_5}")
    print(f"The coverage at alpha = 0.9 is {coverage_0_9}")
    print(f"The mean width at 0.9 is {mean_width_0_9}")
    print(f"The coverage at alpha = 0.95 is {coverage_0_95}")
    print(f"The mean width at 0.95 is {mean_width_0_95}")
    RMSE = math.sqrt(RMSE / len(target_ruls))
    print(f"The RMSE is {RMSE}")
    print(f"The MAE is {MAE / num_predictions}")
    print(f"The mean variance is {mean_var}")
    print(f"The mean std is {mean_std}")

    # Plot the histograms of each relevant prediction.
    # The RUL flight cycles that are plotted may be changed under the "plot_rul_flight_cycles" setting.
    plot_histograms(name, flight_cycles, true_RULs, RUL_distributions, mean_RULs)

    engine_number = 1
    engine = f"{name}_{engine_number}"
    engine_all_predictions = {engine: []}
    engine_mean_prediction = {engine: []}
    engine_true_RUL = {engine: []}

    for prediction in range(num_predictions):
        RUL = target_ruls[prediction]
        all_predictions = [mc_predictions[j][prediction][0] for j in range(number_of_runs)]
        mean_RUL = np.mean(np.array(all_predictions))

        engine_true_RUL[engine] = RUL
        engine_all_predictions[engine] = all_predictions
        engine_mean_prediction[engine] = mean_RUL
        engine_number += 1
        engine = f"{name}_{engine_number}"

    save_prediction_data(f"./data/{name}/{name}", engine_true_RUL, engine_mean_prediction,
                         engine_all_predictions)
