# -*- coding: utf-8 -*-
"""
This script prepares the raw data for the CNN models.

Usage: Place raw data in the ./Data folder and run the script.

"""

# IMPORTS ###

import numpy as np
import pandas as pd


# INPUTS ###
# select the instance of the C-MAPSS dataset you want to process
Instance = "FD004"

print("let's start")

# --------------------------reading in the training data file--------------------------------#
# This consists of engine number, cycle, sensor measurements
cmapss_data_train = pd.read_csv('./data/input/train_' + Instance + '.txt', sep=' ', header=None)
# The last two columns only contain NaN: delete these
cmapss_data_train = cmapss_data_train.iloc[:, 0:-2]  # all rows, all columns excpet last two
# Name the columns
cmapss_data_train.columns = ['ID', 'Cycle', 'O1', 'O2', 'O3', 'S1', 'S2', 'S3', 'S4', 'S5', 'S6', 'S7', 'S8', 'S9',
                             'S10', 'S11', 'S12', 'S13', 'S14', 'S15', 'S16', 'S17', 'S18', 'S19', 'S20', 'S21']
all_sensors = list(cmapss_data_train.columns[5:])

# all_sensors.extend(['Hist opcon 1','Hist opcon 2','Hist opcon 3','Hist opcon 4','Hist opcon 5','Hist opcon 6'])
all_engines = np.unique(cmapss_data_train["ID"])

print("we're downloaded the data!")
# --------------------------Prepare the training data-------------------------#
# include for the training data the max. life and the target RUL values (NOT capped at R_early)
Data_train_life = cmapss_data_train.groupby(['ID'], sort=False)['Cycle'].max().reset_index().rename(
    columns={'Cycle': 'MaxCycleID'})
include_RUL_train = pd.merge(cmapss_data_train, Data_train_life, how='inner', on='ID')
include_RUL_train['RUL'] = include_RUL_train['MaxCycleID'] - include_RUL_train['Cycle']

print("we're doing the ocs!")

# # based on the visual differences, 6 distinct operating regimes are found
# #using the values of the operational settins, each engine can now be given a current operating condition
# operating_condition = []
# for i in range(0,include_RUL_train.shape[0], 1): #for all rows 
#     if include_RUL_train.iloc[i]["O1"] < 5:
#         opco = 1.
#     if include_RUL_train.iloc[i]["O1"] >=5 and include_RUL_train.iloc[i]["O1"] < 15:
#         opco = 2.
#     if include_RUL_train.iloc[i]["O1"] >= 15 and include_RUL_train.iloc[i]["O1"] < 22.5:
#         opco = 3.
#     if include_RUL_train.iloc[i]["O1"] >= 22.5 and include_RUL_train.iloc[i]["O1"] < 30:
#         opco = 4.
#     if include_RUL_train.iloc[i]["O1"] > 30 and include_RUL_train.iloc[i]["O1"] < 38.5:
#         opco = 5.
#     if include_RUL_train.iloc[i]["O1"] >= 38.5:
#         opco = 6.
#     operating_condition.append(opco)
# include_RUL_train["Current opcon"] = operating_condition

# print("we're doing the history!") 

# #add the history as well #Make a dataframe where we add the history history_df = pd.DataFrame(columns = ['ID',
# 'Cycle','Hist opcon 1','Hist opcon 2','Hist opcon 3','Hist opcon 4','Hist opcon 5','Hist opcon 6']) for engine in
# all_engines:

#     #Get the data of this engine
#     engine_df = include_RUL_train.loc[include_RUL_train["ID"]== engine]

# #Initialize the history history = {"ID" : engine, "Cycle" : 0 , 'Hist opcon 1' : 0 , 'Hist opcon 2' : 0 ,
# 'Hist opcon 3' : 0 , 'Hist opcon 4' : 0 , 'Hist opcon 5' : 0 , 'Hist opcon 6' : 0 } #Loop over all cycles for cycle
# in range(1, max(engine_df["Cycle"]) + 1, 1):

#         current_opcon = engine_df.loc[engine_df["Cycle"] == cycle, "Current opcon"]

#         name_column = "Hist opcon " + str(int(current_opcon))

#         history[name_column] = history[name_column] + 1
#         history["Cycle"] = cycle
#         history_df = history_df.append(history, ignore_index = True)


# all_data_train = include_RUL_train.merge(history_df, how = "inner", on = ["ID", "Cycle"])
all_data_train = include_RUL_train

# #HERE COMES THE PART THAT DETERMINES MIN,MAX,MEAN,STD OF EACH COLUMN IN TRAINING DATA THAT NEEDS TO BE USED FOR
# TEST DATA NORMALIZATION
# min_train = all_data_train.groupby('Current opcon').min() max_train =
# all_data_train.groupby('Current opcon').max() mean_train = all_data_train.groupby('Current opcon').mean() std_train
# = all_data_train.groupby('Current opcon').std()

min_train = all_data_train.min()
max_train = all_data_train.max()

for sensor in all_sensors:
    print("Sensor is ", sensor)
    mini = min_train.loc[sensor]
    maxi = max_train.loc[sensor]

    if maxi == mini:
        maxi = mini + 0.0001

    for i in range(0, all_data_train.shape[0], 1):
        # find the opcon
        # opcon = all_data_train.loc[i, "Current opcon"]

        # find the min and max
        # mini = min_train.loc[opcon, sensor]
        # maxi = max_train.loc[opcon, sensor]

        # if maxi == mini:
        #     maxi = mini + 0.0001

        # Min-max
        all_data_train.loc[i, sensor] = ((2 * (all_data_train.loc[i, sensor] - mini)) / (maxi - mini)) - 1

        # z-score
        # std = std_train.loc[opcon, sensor]         
        # mean = mean_train.loc[opcon, sensor] 
        # all_data_train.loc[i, sensor] =  (all_data_train.loc[i, sensor] - mean)/std

fs = 16

# #Make some plots of how sensors change over time 
# for sensor in ["S4"]: #all_sensors:
#     fig, ax = plt.subplots()

# for engine in  [1]: #all_engines: ax.plot(all_data_train.loc[all_data_train["ID"] == engine, "Cycle"],
# all_data_train.loc[all_data_train["ID"] == engine, sensor], marker ="o") #print(cmapss_data_train.loc[
# cmapss_data_train["ID"] == engine, sensor]) ax.set_xlabel("Number of flights", fontsize = fs) ax.set_ylabel("Sensor
# measurement", fontsize = fs)

#         right_side = ax.spines["right"]
#         right_side.set_visible(False)
#         top_side = ax.spines["top"]
#         top_side.set_visible(False)

#         ax.tick_params(axis='x', labelsize=fs - 1  )
#         ax.tick_params(axis='y', labelsize=fs -1)   

#         plt.title("Total temperature at LPT outlet", fontsize = fs)

#         plt.show() 


# #Also normalize the operating conditiosn
# operating_conditions = ['Hist opcon 1','Hist opcon 2','Hist opcon 3','Hist opcon 4','Hist opcon 5','Hist opcon 6']
# for i in range(0, all_data_train.shape[0], 1):
#     #find the opcons
#     opcons = all_data_train.loc[i, operating_conditions]

#     #normalize to 0 and 1 
#     sum_opcons = sum(opcons) 

#     #divide by this 
#     opcons = opcons / sum_opcons 

#     #Replace
#     all_data_train.loc[i, operating_conditions] =  opcons


# drop all columns that are not sensors or operating conditions
all_data_train = all_data_train.drop(columns=['O1', 'O2', 'O3', 'Cycle', 'MaxCycleID'])

print(all_data_train.loc[0, "RUL"])

# train_one = all_data_train.groupby("Current opcon").min()
# train_two = all_data_train.groupby("Current opcon").max()

# #for FD001 and FD003, drop op.con. history as they are single operating condition if Instance == 'FD001' or
# Instance == 'FD003': all_data_train = all_data_train.drop(columns=['Hist opcon 1','Hist opcon 2','Hist opcon 3',
# 'Hist opcon 4','Hist opcon 5','Hist opcon 6'])


# save the fully processed data to test file
np.savetxt(
    r'.\nooc_TRAIN_' + Instance + '_stand_norm.txt',
    all_data_train.values, fmt='%1.6f')

# all_data_train[sensor] = all_data_train.groupby('Current opcon', group_keys=False).apply(lambda g: ((2*(g[i]-g[
# i].min()))/(g[i].max() - g[i].min()))-1)

#######################################################################
# ------------------Test data------------------------------------------#
######################################################################

# %%

# --------------------------reading in the test data file--------------------------------#
cmapss_data_test = pd.read_csv(
    './data/input/test_' + Instance + '.txt', sep=' ',
    header=None)
cmapss_data_test = cmapss_data_test.iloc[:, 0:-2]
cmapss_data_test.columns = ['ID', 'Cycle', 'O1', 'O2', 'O3', 'S1', 'S2', 'S3', 'S4', 'S5', 'S6', 'S7', 'S8', 'S9',
                            'S10', 'S11', 'S12', 'S13', 'S14', 'S15', 'S16', 'S17', 'S18', 'S19', 'S20', 'S21']
test_targets = pd.read_csv('./data/input/RUL_' + Instance + '.txt', header=None)
all_test_engines = np.unique(cmapss_data_test["ID"])

true_RUL = pd.DataFrame(columns=["ID", "RUL"])
for engine in all_test_engines:
    RUL = test_targets.iloc[engine - 1, 0]
    true_RUL = true_RUL.append({"ID": engine, "RUL": RUL}, ignore_index=True)

# Merge them together
cmapss_data_test = cmapss_data_test.merge(true_RUL, how="inner", on=["ID"])

# # based on the visual differences, 6 distinct operating regimes are found
# #using the values of the operational settins, each engine can now be given a current operating condition
# operating_conditions_test = []
# for i in range(0,cmapss_data_test.shape[0], 1): #for all rows 
#     if cmapss_data_test.iloc[i]["O1"] < 5:
#         opco = 1.
#     if cmapss_data_test.iloc[i]["O1"] >=5 and cmapss_data_test.iloc[i]["O1"] < 15:
#         opco = 2.
#     if cmapss_data_test.iloc[i]["O1"] >= 15 and cmapss_data_test.iloc[i]["O1"] < 22.5:
#         opco = 3.
#     if cmapss_data_test.iloc[i]["O1"] >= 22.5 and cmapss_data_test.iloc[i]["O1"] < 30:
#         opco = 4.
#     if cmapss_data_test.iloc[i]["O1"] > 30 and cmapss_data_test.iloc[i]["O1"] < 38.5:
#         opco = 5.
#     if cmapss_data_test.iloc[i]["O1"] >= 38.5:
#         opco = 6.
#     operating_conditions_test.append(opco)
# cmapss_data_test["Current opcon"] = operating_conditions_test


# #Add the history of the operating conditions history_df = pd.DataFrame(columns = ['ID', 'Cycle','Hist opcon 1',
# 'Hist opcon 2','Hist opcon 3','Hist opcon 4','Hist opcon 5','Hist opcon 6']) for engine in all_test_engines:

#     #Get the data of this engine
#     engine_df = cmapss_data_test.loc[cmapss_data_test["ID"]== engine]

# #Initialize the history history = {"ID" : engine, "Cycle" : 0 , 'Hist opcon 1' : 0 , 'Hist opcon 2' : 0 ,
# 'Hist opcon 3' : 0 , 'Hist opcon 4' : 0 , 'Hist opcon 5' : 0 , 'Hist opcon 6' : 0 } #Loop over all cycles for cycle
# in range(1, max(engine_df["Cycle"]) + 1, 1):

#         current_opcon = engine_df.loc[engine_df["Cycle"] == cycle, "Current opcon"]

#         name_column = "Hist opcon " + str(int(current_opcon))

#         history[name_column] = history[name_column] + 1
#         history["Cycle"] = cycle
#         history_df = history_df.append(history, ignore_index = True)


# all_data_test= cmapss_data_test.merge(history_df, how = "inner", on = ["ID", "Cycle"])
all_data_test = cmapss_data_test

for sensor in all_sensors:
    print("Sensor is ", sensor)

    # find the min and max
    mini = min_train.loc[sensor]
    maxi = max_train.loc[sensor]
    if maxi == mini:
        maxi = mini + 0.0001

    for i in range(0, all_data_test.shape[0], 1):
        # find the opcon
        # opcon = all_data_test.loc[i, "Current opcon"]

        all_data_test.loc[i, sensor] = ((2 * (all_data_test.loc[i, sensor] - mini)) / (maxi - mini)) - 1

        # z-score
        # std = std_train.loc[opcon, sensor]
        # mean = mean_train.loc[opcon, sensor]
        # all_data_test.loc[i, sensor] =  (all_data_test.loc[i, sensor] - mean)/std

# #Also normalize the operating conditiosn
# operating_conditions = ['Hist opcon 1','Hist opcon 2','Hist opcon 3','Hist opcon 4','Hist opcon 5','Hist opcon 6']
# for i in range(0, all_data_test.shape[0], 1):
#     #find the opcons
#     opcons = all_data_test.loc[i, operating_conditions]

#     #normalize to 0 and 1 
#     sum_opcons = sum(opcons) 

#     #divide by this 
#     opcons = opcons / sum_opcons 

#     #Replace
#     all_data_test.loc[i, operating_conditions] =  opcons


# drop all columns that are not sensors or operating conditions
all_data_test = all_data_test.drop(columns=['O1', 'O2', 'O3', 'Cycle'])
# test_one = all_data_test.groupby("Current opcon").min()
# test_two = all_data_test.groupby("Current opcon").max()

# # #for FD001 and FD003, drop op.con. history as they are single operating condition if Instance == 'FD001' or
# Instance == 'FD003': all_data_test = all_data_test.drop(columns=['Hist opcon 1','Hist opcon 2','Hist opcon 3',
# 'Hist opcon 4','Hist opcon 5','Hist opcon 6'])


# save the fully processed data to test file
np.savetxt(
    r'.\nooc_TEST_' + Instance + '_stand_norm.txt',
    all_data_test.values, fmt='%1.6f')
