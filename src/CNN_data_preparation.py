# -*- coding: utf-8 -*-

"""Model Data Preparation

This script prepares the test samples and target RUL values for training and testing models on the C-MAPSS dataset.

It includes functions to read input data from CSV files, and process the training and testing data.

"""

import numpy as np
import random as rd
import pandas as pd
import json


# Retrieve relevant, constant values from the settings file.
# For more information on the variables, please take a look at the "CNN_oudel_dropout.py" file.
with open("settings.json", "r") as settings_file:
    settings = json.load(settings_file)

train_model = settings["training"]
test_model = settings["testing"]

r_early = settings["r_early"]

# The names of the input parameters that should be prepared.
# Some sensors may be ommitted later on in the code, this is solely a collection of all names.
input_names = settings["input_parameter_names"]


def input_format_prep(instance, window_size, skipped_sensors):
    """
    Prepares the test samples and target RUL values for model training or testing.

    Parameters
    ----------
    instance : str
        The currently selected CMAPSS subfolder to prepare the data for.
    window_size : int
        The number of past flight cycles included of the selected data subfolder.
    skipped_sensors : list
        The sensors that should be ommitted from the data preparation.

    Returns
    -------
    test_samples : np.ndarray
        The prepared input samples for the CNN model, where each sample is a sequence of sensor measurements over a
        specified window size.
    target_ruls : np.ndarray
        The target RUL values corresponding to each input sample.
    """
    test_samples = []
    target_ruls = []

    if train_model:
        data = prepare_training_data(instance, window_size, skipped_sensors)
        test_samples, target_ruls = data

    elif test_model:
        data = prepare_testing_data(instance, window_size, skipped_sensors)
        test_samples, target_ruls = data

    return test_samples, target_ruls


def prepare_training_data(instance, window_size, skipped_sensors):
    """
    Prepares training data by reading from the specified instance and processing it.

    Parameters
    ----------
    instance : str
        The currently selected CMAPSS subfolder to prepare the data for.
    window_size : int
        The number of past flight cycles included of the selected data subfolder.
    skipped_sensors : list
        The sensors that should be omitted from the data preparation.

    Returns
    -------
    test_samples : np.ndarray
        The prepared input samples for the CNN model, where each sample is a sequence of sensor measurements over a
        specified window size.
    target_ruls : np.ndarray
        The target RUL values corresponding to each input sample.
    """
    input_targets = pd.read_csv(f"./data/input/nooc_TRAIN_{instance}_Input.txt", sep=' ', header=None)
    test_samples, target_ruls = prepare_training_data_model(instance, window_size, skipped_sensors, input_targets)

    return test_samples, target_ruls


def prepare_testing_data(instance, window_size, skipped_sensors):
    """
    Prepares testing data by reading from the specified instance and processing it.

    Parameters
    ----------
    instance : str
        The currently selected CMAPSS subfolder to prepare the data for.
    window_size : int
        The number of past flight cycles included of the selected data subfolder.
    skipped_sensors : list
        The sensors that should be omitted from the data preparation.

    Returns
    -------
    test_samples : np.ndarray
        The prepared input samples for the CNN model, where each sample is a sequence of sensor measurements over a
        specified window size.
    target_ruls : np.ndarray
        The target RUL values corresponding to each input sample.
    """
    input_targets = pd.read_csv(f"./data/input/nooc_TEST_{instance}_Input.txt", sep=' ', header=None)
    test_samples, target_ruls = prepare_testing_data_model(instance, window_size, skipped_sensors, input_targets)

    return test_samples, target_ruls


# this functions prepares the input data from the normalized sensor recordings
def prepare_training_data_model(instance, window_size, skipped_sensors, input_targets):
    """
    Prepares training data from normalized sensor recordings.

    Parameters
    ----------
    instance : str
        The currently selected CMAPSS subfolder to prepare the data for.
    window_size : int
        The number of past flight cycles to include in the data preparation.
    skipped_sensors : list
        The sensors that should be omitted from the data preparation.
    input_targets : Any
        The input and target data read from the CSV file.

    Returns
    -------
    test_samples : np.ndarray
        The prepared input samples for the CNN model, where each sample is a sequence of sensor measurements over a
        specified window size.
    target_ruls : np.ndarray
        The target RUL values corresponding to each input sample.
    """
    seed = 19
    min_size = window_size

    # Retrieve the input names, and drop sensors that are skipped.
    input_targets.columns = input_names
    input_targets = input_targets.drop(columns=skipped_sensors)
    input_targets = input_targets.drop(columns=['Current opcon'])

    all_IDs = np.unique(input_targets["ID"])

    target_ruls = []
    test_samples = []

    for engine_number in all_IDs:
        row_numbers = list(input_targets[input_targets["ID"] == engine_number].index)
        last_index = row_numbers[-1]  # last index where this condition holds.
        first_index = row_numbers[0]  # first index where this condition holds.
        rownumber = first_index - (window_size - min_size)

        while (rownumber + window_size) <= (last_index + 1):
            input_list = []  # the sensor measurements
            current_row = rownumber + window_size

            target_rul = min(input_targets["RUL"].iloc[current_row - 1], r_early)  # exclude current row
            target_ruls.append(target_rul)

            # If there is not sufficient input data, pad all empty columns with zeroes, as explained under Section 3.1
            # "Hyperparameter tuning" in the paper.
            if rownumber < first_index:
                length_missing = first_index - rownumber
                zeroes = [0] * length_missing

                for column in input_targets.columns:
                    if column == "ID" or column == "RUL":
                        continue

                    input_column = list(input_targets[column].iloc[first_index: current_row])  # exclude current row
                    input_column = zeroes + input_column
                    input_list.append(input_column)
            else:
                for column in input_targets.columns:
                    if column == "ID" or column == "RUL":
                        continue

                    input_column = list(input_targets[column].iloc[rownumber: current_row])  # exclude current row
                    input_list.append(input_column)

            rownumber += 1
            test_samples.append(input_list)

    test_samples = np.array(test_samples)
    training_data = []

    # Combine the input samples with their corresponding target RUL.
    for i in range(0, len(test_samples), 1):
        sample = test_samples[i]
        RUL = target_ruls[i]
        training_data.append([sample, RUL])

    # Shuffle training data if we are training the model.
    if train_model:
        rd.seed(seed)
        rd.shuffle(training_data)

    # Append the newly shuffled samples and target RULs to their original lists.
    test_samples, target_ruls = zip(*training_data)

    test_samples = np.array(test_samples)
    target_ruls = np.array(target_ruls)

    return test_samples, target_ruls


def prepare_testing_data_model(instance, window_size, skipped_sensors, input_targets):
    """
    Prepares testing data by processing the input targets and keeping only the end point of the sequence of input samples for each engine.

    Parameters
    ----------
    instance : str
        The currently selected CMAPSS subfolder to prepare the data for.
    window_size : int
        The number of past flight cycles to include in the data preparation.
    skipped_sensors : list
        The sensors that should be omitted from the data preparation.
    input_targets : Any
        The input and target data read from the CSV file.

    Returns
    -------
    test_samples : np.ndarray
        The prepared input samples for the CNN model, where each sample is a sequence of sensor measurements over a
        specified window size.
    target_ruls : np.ndarray
        The target RUL values corresponding to each input sample.
    """
    # Retrieve the input names, and drop sensors that are skipped.
    input_targets.columns = input_names
    input_targets = input_targets.drop(columns=skipped_sensors)
    input_targets = input_targets.drop(columns=['Current opcon'])

    all_IDs = np.unique(input_targets["ID"])

    target_ruls = []
    test_samples = []

    for engine_number in all_IDs:
        row_numbers = list(input_targets[input_targets["ID"] == engine_number].index)
        last_index = row_numbers[-1]  # last index where this condition holds.
        first_index = row_numbers[0]  # first index where this condition holds.
        rownumber = first_index

        while (rownumber + window_size) <= (last_index + 1):
            input_list = []  # the sensor measurements
            current_row = rownumber + window_size

            target_rul = min(input_targets["RUL"].iloc[current_row - 1], r_early)  # exclude current row
            target_ruls.append(target_rul)

            # If there is not sufficient input data, pad all empty columns with zeroes, as explained under Section 3.1
            # "Hyperparameter tuning" in the paper.
            if rownumber < first_index:
                length_missing = first_index - rownumber
                zeroes = [0] * length_missing

                for column in input_targets.columns:
                    if column == "ID" or column == "RUL":
                        continue

                    input_column = list(input_targets[column].iloc[first_index: current_row])  # exclude current row
                    input_column = zeroes + input_column
                    input_list.append(input_column)
            else:
                for column in input_targets.columns:
                    if column == "ID" or column == "RUL":
                        continue

                    input_column = list(input_targets[column].iloc[rownumber: current_row])  # exclude current row
                    input_list.append(input_column)

            rownumber += 1
            test_samples.append(input_list)

    test_samples = np.array(test_samples)

    return test_samples, target_ruls
