# -*- coding: utf-8 -*-

"""Input Validator

This script provides functionality that can be used to validate the various settings that are available for
customisation in the 'settings.json' file found in this repository.

"""


def validate_subfolders(subfolders):
    """
    Ensures that the 'c-mapss_subfolders' setting does not contain any values that do not represent valid C-MAPSS
    dataset subfolders (FD001-FD004). If the function detects that the settings are invalid, it will return an error,
    and exit the program.

    Parameters
    ----------
    subfolders : list
        List of C-MAPSS dataset subfolder indices.
    """
    for f in subfolders:
        if not (1 <= f <= 4):
            raise ValueError("The 'c-mapss_subfolders' setting contains invalid values. The only available dataset"
                             " subfolders are FD001 (1), FD002 (2), FD003 (3), and FD004 (4).")


def validate_exclusivity(train_model, test_model):
    """
    Ensures that the algorithm is set to either train the model, or evaluate the results.

    These settings are mutually exclusive, and one, and only one, must be set to "true" at all times. If the
    function detects that the settings are invalid, it will return an error, and exit the program.

    Parameters
    ----------
    train_model : bool
        Whether or not to train the model.
    test_model : bool
        Whether or not to evaluate the results of the model.
    """
    if train_model is True and test_model is True:
        raise ValueError("Please turn off either the 'training' or 'testing' settings in the 'settings.json' file; "
                         "they are currently both set to 'true'.")
    if train_model is False and test_model is False:
        raise ValueError("Please turn on either the 'training' or 'testing' settings in the 'settings.json' file; "
                         "they are currently both set to 'false'.")


def validate_sensors(all_sensors, sensors_to_use):
    """
    Ensures that the algorithm does not account for any invalid sensors.

    There are a total of 21 sensors in C-MAPSS (S1-S21). Some sensors may optionally not be considered by the algorithm,
    for example if these are deemed redundant.

    Parameters
    ----------
    all_sensors : list
        All errors available to be included.
    sensors_to_use : list
        The sensors that should be included.
    """
    # Ensures that no incorrect sensors have been provided.
    for sensor in all_sensors:
        # Extract the numeric part of the sensor name.
        sensor_number = int(sensor[1:])
        # Check if the sensor number is within the range of 1 to 21.
        if not (1 <= sensor_number <= 21):
            raise ValueError("The 'all_sensors' setting contains invalid values. The only available sensors are S1 up "
                             "until S21.")

    # Ensures that the sensors that will be used are in the 'all_sensors' array.
    for sensor in sensors_to_use:
        if not (sensor in all_sensors):
            raise ValueError("The 'sensors_to_use' setting either contains invalid values, or values that are not in "
                             "the 'all_sensors' setting.")


def validate_settings(settings):
    """
    Validates the 'settings.json' files.

    Various settings are required to abide by certain restrictions to ensure that the algorithm runs smoothly without
    ever running into any input-related errors.

    Parameters
    ----------
    settings : list
        The settings to be validated in a dictionary format, where the keys are the names of the settings, and the
        values are the actual setting values.
    """
    # Retrieve all relevant settings.
    subfolders = settings["c-mapss_subfolders"]
    train_model = settings["training"]
    test_model = settings["testing"]
    sensors_to_use = settings["sensors_to_use"]
    all_sensors = settings["all_sensors"]

    validate_subfolders(subfolders)
    validate_exclusivity(train_model, test_model)
    validate_sensors(all_sensors, sensors_to_use)
