# -*- coding: utf-8 -*-

"""Python Package Installer

This script allows the user to install Python packages required to run the model for the scientific paper "Dynamic
predictive maintenance for multiple components using data-driven probabilistic RUL prognostics: The case of turbofan
engines."

The script requires that `pip` be installed before running this script. For more information, see
https://packaging.python.org/en/latest/tutorials/installing-packages/

Further packages may be added by adding to the "packages" list in this file.

"""

import subprocess
import sys
from importlib import metadata


packages = [
    # The list of packages to be installed.
    'matplotlib',
    'tensorflow',
    'numpy',
    'pandas',
    'tqdm',
    'scikit-learn'
]


def install(package):
    """Install the given package if it is not already installed.

    Parameters
    ----------
    package : str
        The package to be installed.

    """
    try:
        metadata.version(package)
        print(f"{package} is already installed.")
    except metadata.PackageNotFoundError:
        subprocess.check_call([sys.executable, "-m", "pip", "install", package])
        print(f"Successfully installed {package}.")


def main():
    for package in packages:
        install(package)
    print("All packages were installed successfully.")


if __name__ == "__main__":
    main()
