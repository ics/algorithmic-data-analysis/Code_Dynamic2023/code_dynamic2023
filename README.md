# Code_Dynamic2023
This repository consists of the Python code that has been used for the open-access paper "Dynamic predictive maintenance for multiple components using data-driven probabilistic RUL prognostics: The case of turbofan engines" by Mihaela Mitici, Ingeborg de Pater, Anne Barros, and Zhiguo Zeng.

This code processes the condition-monitoring data (CMAPPS) of aircraft turbofan engines to estimate the Remaining Useful Life (RUL) of the engines. It uses a Convolutional Neural Network (CNN) to estimate the RUL of the engines. The data used is openly available from NASA.

## Installation
Before installation, decide whether to use the prepared data, or to prepare your own data from the NASA CMAPPS found here:  https://data.nasa.gov/Aerospace/CMAPSS-Jet-Engine-Simulated-Data/ff5v-kuh6/about_data.

Install a compatible Python3 version, the current recommendation is: `3.12`.

## Usage
The folder "Data" contains 4 folders (FDOO1, FD002, FD003, FD004), each with a data set of engines for training and testing the CNN, see Table 1 and Section 2.1 in the open-source paper "Mitici, Mihaela, Ingeborg de Pater, Anne Barros, and Zhiguo Zeng. "Dynamic predictive maintenance for multiple components using data-driven probabilistic RUL prognostics: The case of turbofan engines." Reliability Engineering & System Safety 234 (2023): 109199".

![Flowchart visualisation of the usage of the Code Dynamic 2023 code base.](https://i.imgur.com/nU8iZko.png)

Step 1: In order to be access the program, first clone the repository from GitLab on your personal computer.

Step 2: Install the following packages using pip (or another package manager), or alternatively, you may use the provided script `install_packages.py` to do so automatically. 

```
 matplotlib   
 tensorflow
 numpy
 pandas
 tqdm
 scikit-learn
```

Step 3: Configure the environment variables available in the `settings.json` file. More information may be under the "Settings" section below.

Step 4: Once you have installed all prerequisites, and configured the settings, `CNN_model_dropout.py` should be run to either train or test the model. This will generate the desired predictions for the Remaining Useful Life (RUL) of the engines, as well as the histograms of the predicted RUL (PDFs).

Step 5: In order to construct the PDF of the RUL of each engine at a given cycle, and optionally the violin plots (the PDF of the RUL of an engine after every cycle), run `construct_PDF_rul.py`.

## File Structure
**data:** 
The data folder contains 1) an input folder with the raw sensor measurements from C-MAPSS (FD001-004), and 2) the predicted RUL for all engines.

**figures:**
After testing the model, the PDF of the RUL of an engine at a specific flight cycle (histograms), and optionally violion plot that shows the PDF of the RUL for an engine after every cycle, can be found here.

**weights:**
While training the model, checkpoints (weights) are saved, i.e., what the model has learnt so far. These files are stored in this folder, and are not human-readable.

Furthermore, the repository contains various scripts and a settings file to realise the program:
- **install_packages.py**: This script can be run to install all required packages for the project.

- **CNN_model_dropout.py**: This script trains the CNN to predict the RUL of the engines in the C-MAPSS dataset (i.e., learns the weights). It also generates the RUL predictions for the test engines.

- **CNN_data_preparation.py**: This script prepares the test samples and target RUL values for training and testing models.

- **construct_PDF_RUL.py**: This script analyses and visualises the RUL predictions of aircraft engines, and processes data from CNN models to evaluate their accuracy.

- **input_validator.py**: This script provides functionality that are used to validate the various settings that are available for customisation in the 'settings.json' file found in this repository.

- **metrics_RUL_predictions.py**: This script provides a suite of functions designed to evaluate the performance and reliability of predictive models of the CNN models from CNN_model_dropout.py.

- **settings.json**: The main settings file that contains various settings to customise input and output of the program.

When extending the functionality, please be aware we use the PEP-8 styling guide found here:
https://peps.python.org/pep-0008/

## Settings
If this is your first time running the program, you should first train the model in order to generate the appropriate weights. After you have generated the weights, the model can now be tested in order to generate the desired output.

The program contains various settings and parameters that are freely adjustable to configure the output provided by the program in the `settings.json` file. A detailed description of each setting is provided in the comments in the `CNN_model_drop.py` script, but a brief overview may be found below:
- `c-mapss_subfolders` - the C-MAPSS subfolder(s) that should be used (e.g., "FD001" = "1").
- `training` - whether to train the model.
- `testing` - whether to test the model.
- `r_early` - the number of cycles of the target RUL; i.e., if the actual RUL is larger than r_early cycles, then the predicted RUL is r_early cycles.
- `dropout_rate` - the dropout rate in each layer of the CNN.
- `window_size` - the number of past flight cycles included of each data subfolder.
- `all_sensors` - all available sensors.
- `sensors_to_use` the sensors that are actually used by the program.
- `batch_size` - the amount of samples propogating through the CNN in one step.
- `no_epochs` - the number of epochs a CNN will make through its training data.
- `no_test_runs` - the number of passes through the neural network.
- `plot_rul_flight_cycles` - the list of instances for which plots of the RUL are generated.
- `input_parameter_names` - the names of the input parameters from C-MAPSS.

## Contact
For questions about the algorithm or paper, please contact [Dr. M.A. Mitici](https://www.uu.nl/medewerkers/MAMitici/).

For questions about the codebase, please open an issue on [Gitlab](https://git.science.uu.nl/ics/algorithmic-data-analysis/Code_Dynamic2023/code_dynamic2023).

## License
[GNU General Public License v3.0](https://choosealicense.com/licenses/gpl-3.0/)

## Citation
"Mitici, Mihaela, Ingeborg de Pater, Anne Barros, and Zhiguo Zeng. "Dynamic predictive maintenance for multiple components using data-driven probabilistic RUL prognostics: The case of turbofan engines." Reliability Engineering & System Safety 234 (2023): 109199"

Also, mention the link to this repository: https://git.science.uu.nl/ics/algorithmic-data-analysis/Code_Dynamic2023/code_dynamic2023

## Author
Mihaela Mitici, Faculty of Science, Utrecht University, Heidelberglaan 8, 3584 CS Utrecht, The Netherlands This code has been used for the open-source paper "Mitici, Mihaela, Ingeborg de Pater, Anne Barros, and Zhiguo Zeng. "Dynamic predictive maintenance for multiple components using data-driven probabilistic RUL prognostics: The case of turbofan engines."
